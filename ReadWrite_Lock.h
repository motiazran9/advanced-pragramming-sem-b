#ifndef _INCLUDED_READER_H
#define _INCLUDED_READER_H

#include <fstream>
#include <string>
#include <mutex>
#include <condition_variable>

using std::condition_variable;
using std::ifstream;
using std::ofstream;
using std::string;
using std::mutex;

class ReadWrite_Lock
{
	public:
		ReadWrite_Lock(string filename);
		void lockRead();
		void unlockRead();
		void lockWrite();
		void unlockWrite();

	private:
		ofstream file;
		unsigned int noOfReaders;
		mutex l;
		std::unique_lock<mutex> lock;
		condition_variable cv;
};

#endif