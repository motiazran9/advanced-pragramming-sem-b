#include <iostream>
#include <thread>
#include "ReadWrite_Lock.h"

using std::cout;
using std::endl;
using std::thread;

ReadWrite_Lock rwLock("Gal.txt");

mutex l;

void readFromFile(ifstream& f)
{
	char s[6];

	rwLock.lockRead();
	f.read(s, 5);
	rwLock.unlockRead();
}

void writeToFile(ofstream& f)
{
	rwLock.lockWrite();
	f << "Moti Azran";
	rwLock.unlockWrite();
}

int main()
{
	ofstream f1("Gal.txt");
	ifstream f2("Gal.txt");
	
	thread t1(writeToFile, std::ref(f1));
	thread t2(writeToFile, std::ref(f1));
	thread t3(writeToFile, std::ref(f1));

	thread t4(readFromFile, std::ref(f2));
	thread t5(readFromFile, std::ref(f2));
	thread t6(readFromFile, std::ref(f2));

	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	t6.join();
		
	f1.close();
	f2.close();

	std::cin.get();
	return 0;
}