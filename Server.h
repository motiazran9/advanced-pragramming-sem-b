#ifndef _SERVER_H_INCLUDED
#define _SERVER_H_INCLUDED

#include <string>
#include <exception>
#include <WinSock2.h>
#include <Windows.h>

using std::string;

class Server
{
	public:
		Server(const short port);
		~Server();
		SOCKET acceptNewConnection();
		void operator ()(SOCKET s);
		static bool ipCheck(string ip);
		static bool portCheck(short port);
		static void sendPacket(SOCKET s, string str);
		static string recvPacket(SOCKET s, int size);

	private:
		SOCKET listenSocket;
		WSADATA info;
		sockaddr_in service;
		sockaddr_in fromService;
};

class ServerException : public std::exception
{
	public:
		ServerException(string error) : error(error) {}
		virtual const char* what() const throw() { return error.c_str(); }

	private:
		string error;
};

#endif