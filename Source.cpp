#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <iostream>
#include <string>
#include <WinSock2.h>
#include <Windows.h>

#define LEN 9
#define NUM_OF_DOTS 3
#define MIN_IP_NUM 0
#define MAX_IP_NUM 255
#define MIN_IPV4_LEN 7
#define MAX_IPV4_LEN 15
#define MIN_PORT 0
#define MAX_PORT 65535

using std::string;
using std::cout;
using std::cin;
using std::endl;

bool ipCheck(string ip);
bool portCheck(int port);
SOCKET createServer(string ip, int port);

int main()
{
	string ip;
	int port;

	do
	{
		cout << "Please enter your ip: ";
		cin >> ip;
	} while (!ipCheck(ip));

	do
	{
		cout << "Please enter port to listen on: ";
		cin >> port;
	} while (!portCheck(port));

	SOCKET s = createServer(ip, port);

	if (send(s, "Accepted\0", LEN, 0) == SOCKET_ERROR)
	{
		cout << "send error number: " << WSAGetLastError() << endl;
	}

	shutdown(s, SD_BOTH);
	closesocket(s);

	return 0;
}

bool ipCheck(string ip)
{
	if (ip.length() < MIN_IPV4_LEN || ip.length() > MAX_IPV4_LEN)
	{
		// to long or to short addres
		return false;
	}
	else if (count(ip.begin(), ip.end(), '.') != NUM_OF_DOTS)
	{
		// to less or to more dots in the address
		return false;
	}
	else
	{
		string num;
		for (unsigned int i = 0; i < ip.length(); ++i)
		{
			if (ip[i] >= '0' && ip[i] <= '9')
			{
				num += ip[i];
			}
			else if (ip[i] == '.')
			{
				if (atoi(num.c_str()) < MIN_IP_NUM || atoi(num.c_str()) > MAX_IP_NUM)
				{
					// the number in the IP address is to big or to small
					return false;
				}
				num = ""; // clean the string
			}
			else
			{
				// invalid character in the IP address
				return false; 
			}
		}
	}

	return true;
}

bool portCheck(int port)
{
	 if (port < MIN_PORT || port > MAX_PORT)
	 {
		 // Port out of bounds
		 return false;
	 }

	 return true;
}

SOCKET createServer(string ip, int port)
{
	WSADATA info;
	SOCKET listenSocket, acceptSocket;
	sockaddr_in service;

	if (WSAStartup(MAKEWORD(2, 0), &info) != 0)
	{
		cout << "WSAStartup error number: " << WSAGetLastError() << endl;
	}

	if ((listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
	{
		cout << "socket error number: " << WSAGetLastError() << endl;
		WSACleanup();
	}

	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr(ip.c_str());
	service.sin_port = htons(port);

	if (bind(listenSocket, (SOCKADDR*)&service, sizeof(service)) == SOCKET_ERROR)
	{
		cout << "bind error number: " << WSAGetLastError() << endl;
		closesocket(listenSocket);
		WSACleanup();
	}

	if (listen(listenSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		cout << "listen error number: " << WSAGetLastError() << endl;
		closesocket(listenSocket);
		WSACleanup();
	}

	if ((acceptSocket = accept(listenSocket, NULL, NULL)) == SOCKET_ERROR)
	{
		cout << "accept error number: " << WSAGetLastError() << endl;
		closesocket(listenSocket);
		WSACleanup();
	}

	closesocket(listenSocket);

	return acceptSocket;
}