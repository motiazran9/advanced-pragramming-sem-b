#include <iostream>
#include <vector>
#include <thread>
#include "Server.h"

#pragma comment (lib, "Ws2_32.lib")

using std::cout;
using std::endl;
using std::cin;
using std::vector;
using std::thread;

void clientHandler(SOCKET s);

int main()
{
	vector<thread*> threads;
	int port;

	do
	{
		cout << "Please enter port to listen on: ";
		cin >> port;
	} while (!Server::portCheck(port));

	try
	{
		Server server(port);

		SOCKET s;
		while (true)
		{
			s = server.acceptNewConnection();
			threads.push_back(new thread(clientHandler, s));
		}
	}
	catch (ServerException& e)
	{
		cout << endl << e.what() << endl;
	}
	catch (...)
	{
		cout << endl << "Unknown error" << endl;
	}

	for (unsigned int i = 0; i < threads.size(); ++i)
	{
		threads[i]->join();
	}

	system("PAUSE");
	return 0;
}

void clientHandler(SOCKET s)
{
	try
	{
		Server::sendPacket(s, "Accepted!");
	}
	catch (ServerException& e)
	{
		cout << endl << e.what() << endl;
	}

	closesocket(s);
}