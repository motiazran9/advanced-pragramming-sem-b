#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "Server.h"
#include <iostream>

using std::cout;
using std::endl;

#define NUM_OF_DOTS 3
#define MIN_IP_NUM 0
#define MAX_IP_NUM 255
#define MIN_IPV4_LEN 7
#define MAX_IPV4_LEN 15
#define MIN_PORT 0
#define MAX_PORT 65535

#pragma comment(lib,"ws2_32.lib")

bool Server::ipCheck(string ip)
{
	if (ip.length() < MIN_IPV4_LEN || ip.length() > MAX_IPV4_LEN)
	{
		// to long or to short addres
		return false;
	}
	else if (count(ip.begin(), ip.end(), '.') != NUM_OF_DOTS)
	{
		// to less or to more dots in the address
		return false;
	}
	else
	{
		string num;
		for (unsigned int i = 0; i < ip.length(); ++i)
		{
			if (ip[i] >= '0' && ip[i] <= '9')
			{
				num += ip[i];
			}
			else if (ip[i] == '.')
			{
				if (atoi(num.c_str()) < MIN_IP_NUM || atoi(num.c_str()) > MAX_IP_NUM)
				{
					// the number in the IP address is to big or to small
					return false;
				}
				num = ""; // clean the string
			}
			else
			{
				// invalid character in the IP address
				return false;
			}
		}
	}

	return true;
}

bool Server::portCheck(short port)
{
	if (port < MIN_PORT || port > MAX_PORT)
	{
		// Port out of bounds
		return false;
	}

	return true;
}

Server::Server(const short port)
{
	if (WSAStartup(MAKEWORD(2, 0), &info) != 0)
	{
		throw ServerException("WSAStartup error");
	}

	if ((listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
	{
		WSACleanup();
		throw ServerException("Socket error");
	}

	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr("127.0.0.1");
	service.sin_port = htons(port);

	if (bind(listenSocket, (SOCKADDR*)&service, sizeof(service)) == SOCKET_ERROR)
	{
		closesocket(listenSocket);
		WSACleanup();
		throw ServerException("Bind error");
	}

	if (listen(listenSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		closesocket(listenSocket);
		WSACleanup();
		throw ServerException("Listen error");
	}

	std::cout << "listening to port " << htons(service.sin_port) << " ..." << std::endl;
}

Server::~Server()
{
	closesocket(listenSocket);
	WSACleanup();
}

SOCKET Server::acceptNewConnection()
{
	SOCKET acceptSocket;
	int s = sizeof(fromService);

	if ((acceptSocket = accept(listenSocket, (struct sockaddr*)&fromService, &s)) == SOCKET_ERROR)
	{
		closesocket(listenSocket);
		throw ServerException("Accept error");
	}

	cout << "Server connect to: " << inet_ntoa(fromService.sin_addr) << endl;

	return acceptSocket;
}

string Server::recvPacket(SOCKET s, int size)
{
	string str;
	char *buf = new char[size];

	if (recv(s, buf, size, 0) == SOCKET_ERROR)
	{
		throw ServerException("Recv error");
	}

	str = buf;

	return str;
}

void Server::sendPacket(SOCKET s, string buf)
{
	if (send(s, buf.c_str(), buf.length(), 0) == SOCKET_ERROR)
	{
		throw ServerException("Send error");
	}
}