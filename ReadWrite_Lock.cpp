#include "ReadWrite_Lock.h"

bool rPresent;
bool hasChar;

bool isFileEmpty()
{
	return !hasChar;
}

bool noReaders()
{
	return !rPresent;
}

ReadWrite_Lock::ReadWrite_Lock(string filename) : noOfReaders(0), file(filename), lock(l)
{
	rPresent = false;
	hasChar = file.end == file.beg;
	lock.unlock();
}

void ReadWrite_Lock::lockRead()
{
	lock.lock();

	while (file.beg == file.end) // loop while file is empty
	{
		cv.wait(lock, isFileEmpty);
	}

	++noOfReaders;
}

void ReadWrite_Lock::unlockRead()
{
	--noOfReaders;

	if (noOfReaders == 0)
	{
		cv.notify_one();
	}

	lock.unlock();
}

void ReadWrite_Lock::lockWrite()
{
	lock.lock();

	while (noOfReaders > 0)
	{
		cv.wait(lock, noReaders);
	}
}

void ReadWrite_Lock::unlockWrite()
{
	hasChar = false;

	lock.unlock();
	cv.notify_all();
}